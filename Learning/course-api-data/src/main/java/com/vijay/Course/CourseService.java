package com.vijay.Course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {
	
	private List<Course> topics = 
		new ArrayList<>(Arrays.asList(new Course("spring","Spring Framework","Spring Framework Desc"),
				new Course("java","Java Language","Java Language Desc"),
				new Course("j2ee","J2EE framework","J2EE framework Desc"))); 
							

@Autowired
private CourseRepository courseRepository;

public List<Course> getAllTopics(String id)	{ 
	
	List<Course> courses = new ArrayList<>();
	
	courseRepository.findAll().forEach(topics::add);
	
	return topics;
	}

public  Optional<Course> getTopic(String id) {

	/*
 int i =0;
	for( i=0; i<topics.size();i++) 
		if(topics.get(i).getId().equals(id)) 
		 break;
	
	return(topics.get(i)); */
	return topicRepository.findById(id);
}

public void addTopic(Course t) { 
	
//topics.add(t);
	topicRepository.save(t);
}

public void updateTopic(String id,Course t) {
 
/*	for(int i=0; i<topics.size();i++) 
		if(topics.get(i).getId().equals(id)) 
				topics.set(i, t); */
	topicRepository.save(t);
		return;
	
  }

public void deleteTopic(String id) {
	// topics.remove(id);
	topicRepository.deleteById(id);
}

}
