package com.vchillara.moviecatalogservice.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.vchillara.moviecatalogservice.model.CatalogItem;
import com.vchillara.moviecatalogservice.model.Movie;
import com.vchillara.moviecatalogservice.model.Rating;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResource {
	
	@Autowired
	private RestTemplate restTemplate; 
	
	@RequestMapping("/{userId}")
	public List<CatalogItem> getCatalog(@PathVariable("userId") String userId) {
		
		
		List<CatalogItem> ciList = new ArrayList<CatalogItem>();
		List<Rating> ratings = Arrays.asList(new Rating("Transformers", 4), new Rating("Titanic", 3));
		
		for(int i=0;i < ratings.size(); i++) {
			Rating rating = ratings.get(i);
			Movie movie = restTemplate.getForObject("http://movie-info-service/movies/"+ rating.getMovie_id(),Movie.class);
			CatalogItem ci = new CatalogItem(movie.getName(),"Test Desc", rating.getRating());
			ciList.add(ci);
		}
			return ciList;
		}
	}

