package com.vchillara.moviecatalogservice.model;

public class Rating {

	String movie_id;
	int rating;
	
	public Rating(String movie_id, int rating) {
		super();
		this.movie_id = movie_id;
		this.rating = rating;
	}
	public String getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(String movie_id) {
		this.movie_id = movie_id;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}

	
}
