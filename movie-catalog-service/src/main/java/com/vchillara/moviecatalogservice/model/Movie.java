package com.vchillara.moviecatalogservice.model;

public class Movie {

	public Movie(String id, String name) {
		super();
		this.id = id;
		this.name=name;
	}

	public Movie() {
		super();
	}

	private String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private String name;
}
