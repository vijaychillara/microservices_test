package com.vchillara.movieinfoservice.resources;

import java.util.Collections;
import java.util.List;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vchillara.movieinfoservice.model.Movie;

@RestController
@EnableEurekaClient
@RequestMapping("/movies")
public class MovieResource {

	@RequestMapping("/{movieId}")
	public Movie getMovieInfo(@PathVariable("movieId") String movieId) {
		
return  new Movie(movieId,"Test Movie");
	}
}
